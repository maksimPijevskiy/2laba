import requests
import hashlib
import urllib
import falcon
import sqlite3
import base64
import dateutil.parser
from datetime import datetime
from waitress import serve


class GravatarCachedResource:
    def __init__(self):
        self.connection = sqlite3.connect('database.db')
        self.cursor = self.connection.cursor()
        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS 'cache'(
            `Id` TEXT NOT NULL,
            `CreateDataTime` TEXT NOT NULL,
            `Data` BLOB,
            PRIMARY KEY (`Id`)
        );
        ''')

    def get_username_hash(self, username):
        return hashlib.md5(username.encode('utf-8')).hexdigest()

    def get_gravatar_url(self, username):
        gravatar_url = "https://www.gravatar.com/avatar/" + self.get_username_hash(username) + "?"
        gravatar_url += urllib.parse.urlencode({'s': str(300)})
        return gravatar_url

    def get_gravatar_image(self, username):
        url = self.get_gravatar_url(username)
        return requests.get(url).content

    def get_current_datetime(self):
        return datetime.now().isoformat()

    def parse_datetime(self, date):
        return dateutil.parser.parse(date)

    def save_image_to_database(self, username):
        username_hash = self.get_username_hash(username)
        image = self.get_gravatar_image(username)
        current_datetime = self.get_current_datetime()

        if self.get_image_from_database(username)is None:
            self.cursor.execute("INSERT INTO `cache` VALUES (?,?,?);", (username_hash, current_datetime, image,))
        else :
            self.cursor.execute("UPDATE `cache` SET `CreateDataTime` = ? , `data` = ? WHERE `Id` = ? ;",(current_datetime,image,username_hash))

        self.connection.commit()

    def get_image_from_database(self, username):
        username_hash = self.get_username_hash(username)
        self.cursor.execute("SELECT * FROM `cache` WHERE `cache`.`Id`='{}'".format(username_hash))
        return self.cursor.fetchone()

    def get_image_from_cache_or_download(self, username):
        data = self.get_image_from_database(username)
        if data:
            now = datetime.now()
            created_at = self.parse_datetime(data[1])
            delta_in_seconds = (now - created_at).total_seconds()

            if delta_in_seconds < 15:
                print('Image download from cache')
                return data[2]

        print('Image download from Site')
        self.save_image_to_database(username)
        return self.get_gravatar_image(username)


    def on_get(self, req, resp, username):
        self.connection =sqlite3.connect('database.db')
        self.cursor = self.connection.cursor()

        image = self.get_image_from_cache_or_download(username)
        encoded = base64.b64encode(image).decode("utf-8")

        resp.content_type = 'text/html'
        resp.body = "<img src = 'data:image/png;base64,{}'>".format(encoded)
        resp.status = falcon.HTTP_200

api = falcon.API()
api.add_route('/api/avatar/{username}', GravatarCachedResource())

# GravatarCachedResource().save_image_to_database('lyftzeigen5')
#GravatarCachedResource().get_image_from_cache_or_download('lyftzeigen7')

# print(GravatarCachedResource().get_userrname_hash(('lyftzeigen')))
# print(GravatarCachedResource().get_gravatar_url('lyftzeigen'))
# print(GravatarCachedResource().get_gravatar_image('lyftzeigen'))
# print(GravatarCachedResource().get_current_datetime())
# print(GravatarCachedResource().parse_datetime('2019-09-06T12:23:00.000000'))
#print(GravatarCachedResource().get_image_from_database('lyftzeigen2'))
serve(api, host='127.0.0.1', port=8000)
